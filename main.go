package HearX

import (
"log"
"net/smtp"
	_ "encoding/json"
)

type ObjactValues struct {
	Subject string
	Body string
	EmailAddress string
	Username string
	Password string
}

func main() {

	obj := ObjactValues{} // initializing object

	auth := smtp.PlainAuth("", obj.Username, obj.Password, "smtp.mailtrap.io") // setup of smtp auth

	// creating of email object to be sent out
	to := []string{obj.EmailAddress}
	msg := []byte(obj.EmailAddress +
		"Subject:" + obj.Subject +
		"\r\n" +
		obj.Body)
	//sending email out
	err := smtp.SendMail("https://sendgrid.com/solutions/email-api", auth, "dummy@gmail.com", to, msg)
	if err != nil {
		log.Fatal(err)
	}
}
